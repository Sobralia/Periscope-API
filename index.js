var	request = require('request');
var	cheerio = require('cheerio');
var	fs = require('fs');
var exec = require('child_process').exec;

var	url = {
	profile: "https://www.periscope.tv/",
	broadcast: "https://api.periscope.tv/api/v2/accessVideoPublic?broadcast_id="
};

var	PeriscopeApi = {
	getProfileInfo: function(profile_name, callback){
		var	request_url = url.profile + profile_name;

		request(request_url, function(error, response, body){
			if (error){
				callback(error);
				return;
			}
			if (response.statusCode != 200){
				callback(new Error("Http wrong statuscode: " + response.statusCode));
				return;
			}

			var	$ = cheerio.load(body);
			var	profile = {
				info: null,
				lastBroadcast: null,
				broadcasts: null
			};

			if ($("#page-container").length){
				var	raw = $("#page-container").data("store");
				var	broadcasts_list = raw.BroadcastCache.broadcasts;
				var	broadcasts_ids = raw.BroadcastCache.broadcastIds;

				if (!!raw && !!raw.User && !!raw.User.user){
					profile.info = raw.User.user;
				}
				if (!!broadcasts_list && !! broadcasts_ids && !!broadcasts_list[broadcasts_ids[0]]){
					profile.lastBroadcast = broadcasts_list[broadcasts_ids[0]].broadcast.data;
					profile.broadcasts = raw.BroadcastCache;
				}
			}
			fs.writeFile(profile.info.username + "_raw.js", JSON.stringify(raw.BroadcastCache));
			callback(null, profile);
		});
	},
	getBroadcastInfo: function(broadcastId, callback){
		var	request_url = url.broadcast + broadcastId;
		request(request_url, function(error, response, body){
			if (error){
				callback(error);
				return;
			}

			if (response.statusCode != 200){
				callback(new Error("Request error status: " + response.statusCode))
				return;
			}

			var	broadcasts_info = JSON.parse(body);
			
			callback(null, broadcasts_info);
		});
	},
	recordBroadcast: function(broadcast_id, path, callback){
		var	request_url = url.broadcast + broadcast_id;

		this.getBroadcastInfo(broadcast_id, function(error, broadcasts_info){
			if (error){
				callback(error);
				return;
			}

			var	replay_url = broadcasts_info.hls_url;
			var	cmd = "livestreamer \"hls://" + replay_url + "\" best -o \"" + path + "\"";
			fs.stat(path, function(err, stat){
				if (err == null){
					callback(new Error("File already exist"));
					return;
				}
				if (err && err.code != "ENOENT"){
					callback(err);
					return;
				}
				exec(cmd, function(error, stdout, stderr) {
					if (error){
						callback(error);
						return;
					}
					callback(null, broadcasts_info);
				});
			});
		});
	},
	downloadBroadcast: function(broadcast_id, path, callback){
		var	request_url = url.broadcast + broadcast_id;

		this.getBroadcastInfo(broadcast_id, function(error, broadcasts_info){
			if (error){
				callback(error);
				return;
			}

			var	replay_url = broadcasts_info.replay_url;
			var	the_cookie = "CloudFront-Policy=" + broadcasts_info.cookies[0].Value +
			"; CloudFront-Signature=" + broadcasts_info.cookies[1].Value +
			"; CloudFront-Key-Pair-Id=" + broadcasts_info.cookies[2].Value;
			var	cmd = "livestreamer --http-cookie \"" + the_cookie + "\" \"hls://" + replay_url + "\" best -o \"" + path + "\"";

			fs.stat(path, function(err, stat){
				if (err == null){
					callback(new Error("File already exist"));
					return;
				}
				if (err && err.code != "ENOENT"){
					callback(err);
					return;
				}
				exec(cmd, function(error, stdout, stderr) {
					if (error){
						callback(error);
						return;
					}
					callback(null, broadcasts_info);
				});
			});
		});
	}
};

module.exports = PeriscopeApi;
